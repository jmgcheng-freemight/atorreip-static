$(document).ready(function(){
	/**/
	var o_form = $('#frm_contact');
	var o_popup_inner = $('.popup-inner');
	var o_popup_close = $('.popup-close-link');
	
	
	/*
		scroll to top
	*/
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) 
		{
			$('.page-top').fadeIn();
		} else {
			$('.page-top').fadeOut();
		}
	});
	$('.page-top').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	
	
	/*
		popup
	*/
	// settings
	set_popup_inner_size();
	window.addEventListener('resize', set_popup_inner_size);
	function set_popup_inner_size()
	{
		var i_popup_inner_size = window.innerHeight - 50;
		o_popup_inner.css('height', i_popup_inner_size + 'px');
	}
	o_popup_inner.on('click', function(e){
		if (e.target == this) { 
			$(this).parent('.popup').addClass('is-hidden');
        }
	});
	o_popup_close.on('click', function(e){
		e.preventDefault();
		$(this).parents('.popup').addClass('is-hidden');
	});
	// contact popup
	$('.contact-btn-popup').on('click', function(){
		if( !o_form.parents('.popup').is(':visible') )
		{
			o_form.parents('.popup').removeClass('is-hidden');
		} 
		else 
		{  
			o_form.parents('.popup').addClass('is-hidden');
		}
	});
	
	
	/*
		getDate()
	*/
	function getDate() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
		dd='0'+dd
		} 
		if(mm<10) {
		mm='0'+mm
		} 
		today = mm+'/'+dd+'/'+yyyy;
		return today;
	}
	
	
	/*
		g script - API
	// Your Client ID can be retrieved from your project in the Google
	var CLIENT_ID = '612891148009-nps8d5m4d9ii89lsio8qruhk74pprtdt.apps.googleusercontent.com'; 
	var SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
	function checkAuth() {
		gapi.auth.authorize(
		{
		'client_id': CLIENT_ID,
		'scope': SCOPES.join(' '),
		'immediate': true
		}, handleAuthResult);
	}
	function handleAuthResult(authResult) {
		if (authResult && !authResult.error) 
		{
			pushFormData();
		} 
		else 
		{
			console.log('Auth Failed');
		}
	}
	*/

	
	/*
		pushFormData()
	*/
	function pushFormData() {
		console.log('pushFormData');
		// Acquire this from the Apps Script editor,
		// under Publish > Deploy as API executable.
		var scriptId = "MU3QI7PlYlJaSvO_62hQ1MSJ51K4EHMtn"; 
		// Initialize parameters for function call.
		var o_params = {
			'time_stamp':getDate(),
			'name':'Test Name',
			'sex':'Test Sex',
			'age':'Test Age',
			'consider_dormitory_start':'-',
			'consider_months_weeks_stay':'-',
			'current_english_level':'-',
			'email_address':'-',
			'email_address_confirmation':'-',
			'another_email_address':'-',
			'another_email_address_confirmation':'-',
			'already_stayed_ph':'-'
		};
		// Create execution request.
		var request = {
			'function': 'addResponse',
			'parameters': [o_params],
			'devMode': true
		};
		// Make the request.
		var op = gapi.client.request({
			'root': 'https://script.googleapis.com',
			'path': 'v1/scripts/' + scriptId + ':run',
			'method': 'POST',
			'body': request
		});
		// Log the results of the request.
		op.execute(function(resp) {
		  if (resp.error && resp.error.status) {
			// The API encountered a problem before the script started executing.
			console.log('Error calling API: ' + JSON.stringify(resp, null, 2));
		  } else if (resp.error) {
			// The API executed, but the script returned an error.
			var error = resp.error.details[0];
			console.log('Script error! Message: ' + error.errorMessage);
		  } else {
			// Here, the function returns an array of strings.
			console.log('Result: ' + resp);
			alert(resp);
		  }
		});
	}
	
	
	/*
	*/
	$('#frm_contact_submit_backup').on('click', function(e){
		e.preventDefault();
		console.log('Push');
		//
		var a_fields = $('#frm_contact').serializeArray();
		var o_params_temp = {};
		o_params_temp['frm_contact_time_stamp'] = getDate();
		a_fields.forEach( function (o_item)
		{
			o_params_temp[ o_item.name ] = o_item.value;
		});
		//
		var o_params = {
			'time_stamp':o_params_temp.frm_contact_time_stamp,
			'name':o_params_temp.frm_contact_name,
			'sex':o_params_temp.frm_contact_sex,
			'age':o_params_temp.frm_contact_age,
			'consider_dormitory_start':o_params_temp.frm_contact_consider_dormitory_start,
			'consider_months_weeks_stay':o_params_temp.frm_contact_consider_months_weeks_stay,
			'current_english_level':o_params_temp.frm_contact_current_english_level,
			'email_address':o_params_temp.frm_contact_email_address,
			'email_address_confirmation':o_params_temp.frm_contact_email_address_confirmation,
			'another_email_address':o_params_temp.frm_contact_another_email_address,
			'another_email_address_confirmation':o_params_temp.frm_contact_another_email_address_confirmation,
			'already_stayed_ph':o_params_temp.frm_contact_already_stayed_ph
		};
		console.log( o_params );
		//

		/*
		$.ajax({
			url: "https://script.google.com/macros/s/AKfycbzBSNRkxxl0iL-z4opdZUASV2MwtsryAZdcdN-P8J9Bqur2ufY/exec?prefix=?",
			dataType: "jsonp",
			data: o_params,
			success: function( response ) {
				console.log( response );
			}
		});
		*/
		
		
		

	});
	
	$('.btn-test-push-form-data').on('click', function(e){

		/*
		var url = 'https://script.google.com/macros/s/AKfycbzBSNRkxxl0iL-z4opdZUASV2MwtsryAZdcdN-P8J9Bqur2ufY/exec?prefix=myabc';
		$.ajax({
		  type: "GET",
		  url: url,
		  data: {callback:"?"},
		  dataType: "script"
		  }).done( function( data ) {
				console.log('test');
				 console.log(data); //data returned : UNDEFINED
		});
			
		$.getJSON( 
			'https://script.google.com/macros/s/AKfycbzBSNRkxxl0iL-z4opdZUASV2MwtsryAZdcdN-P8J9Bqur2ufY/dev?prefix=?',
			null,
			function( results ) {
				console.log(results);
			}
		);
		
		var o_params = {
			'time_stamp':getDate(),
			'name':'Test Name',
			'sex':'Test Sex',
			'age':'Test Age',
			'consider_dormitory_start':'-',
			'consider_months_weeks_stay':'-',
			'current_english_level':'-',
			'email_address':'-',
			'email_address_confirmation':'-',
			'another_email_address':'-',
			'another_email_address_confirmation':'-',
			'already_stayed_ph':'-'
		};	
		$.ajax({
			url: "https://script.google.com/macros/s/AKfycbzBSNRkxxl0iL-z4opdZUASV2MwtsryAZdcdN-P8J9Bqur2ufY/dev?prefix=?",
			// The name of the callback parameter, as specified by the YQL service
			//jsonp: "callback",
			// Tell jQuery we're expecting JSONP
			dataType: "jsonp",
			// Tell YQL what we want and that we want JSON
			data: o_params,

			// Work with the response
			success: function( response ) {
				console.log( response ); // server response
			}
		});
		
		$.ajax({
			url: "https://script.google.com/macros/s/AKfycbzBSNRkxxl0iL-z4opdZUASV2MwtsryAZdcdN-P8J9Bqur2ufY/exec", 
			method: "POST",
			mozSystem: true, 
			success: function(result){
				console.log( result );
			}
		});
		*/
	});
	
});





var formApp = angular.module('formApp', []);
formApp.controller('formController', ['$scope', function($scope) {
	
	$scope.getDate = function() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10) {
		dd='0'+dd
		} 
		if(mm<10) {
		mm='0'+mm
		} 
		today = mm+'/'+dd+'/'+yyyy;
		return today;
	};
	
	
	$scope.submit = function(frm_contact) {
		var b_spreadSheetSent = false;
		var b_emailSent = false;
		var a_fields = $('#frm_contact').serializeArray();
		var o_params_temp = {};
		o_params_temp['frm_contact_time_stamp'] = $scope.getDate();
		a_fields.forEach( function (o_item)
		{
			o_params_temp[ o_item.name ] = o_item.value;
		});
		//
		var o_params = {
			'time_stamp':o_params_temp.frm_contact_time_stamp,
			'name':o_params_temp.frm_contact_name,
			'sex':o_params_temp.frm_contact_sex,
			'age':o_params_temp.frm_contact_age,
			'consider_dormitory_start':o_params_temp.frm_contact_consider_dormitory_start,
			'consider_months_weeks_stay':o_params_temp.frm_contact_consider_months_weeks_stay,
			'current_english_level':o_params_temp.frm_contact_current_english_level,
			'email_address':o_params_temp.frm_contact_email_address,
			'email_address_confirmation':o_params_temp.frm_contact_email_address_confirmation,
			'another_email_address':o_params_temp.frm_contact_another_email_address,
			'another_email_address_confirmation':o_params_temp.frm_contact_another_email_address_confirmation,
			'already_stayed_ph':o_params_temp.frm_contact_already_stayed_ph
		};
		console.log( o_params );
		
		//push to google spreadSheet
		$.ajax({
			url: "https://script.google.com/macros/s/AKfycbzBSNRkxxl0iL-z4opdZUASV2MwtsryAZdcdN-P8J9Bqur2ufY/exec?prefix=?",
			dataType: "jsonp",
			data: o_params,
			beforeSend: function() {
				$('.form-contact-ajax-msg').hide();
				$('.form-contact-ajax-msg .form-contact-ajax-msg-sending').hide();
				$('.form-contact-ajax-msg .form-contact-ajax-msg-sent').hide();
				$('.form-contact-ajax-msg .form-contact-ajax-msg-error').hide();
				$('.form-contact-submit-btn').hide();
				
				$('.form-contact-ajax-msg').show();
				$('.form-contact-ajax-msg .form-contact-ajax-msg-sending').show();
			},
			success: function( o_response_sheet ) {
				console.log( o_response_sheet );
				if( o_response_sheet.status == true )
				{
					b_spreadSheetSent = true;
					
					if( b_spreadSheetSent )
					{
						//try sending email notification
						$.ajax({
							url: "mail-notification.php",
							type: "POST",
							dataType: "json",
							data: o_params,
							success: function( o_response_email ) {
								console.log( o_response_email );				
								
								if( o_response_email.result == true )
								{
									b_emailSent = true;
									$('.form-contact-submit-btn').hide();
									$('.form-contact-ajax-msg .form-contact-ajax-msg-sending').hide();
									$('.form-contact-ajax-msg .form-contact-ajax-msg-sent').show();
								}
								else
								{
									console.log('fail email');
									$('.form-contact-ajax-msg').hide();
									$('.form-contact-ajax-msg .form-contact-ajax-msg-sending').hide();
									$('.form-contact-submit-btn').show();
								}
							}
						});
					}
					else
					{
						console.log('fail sheet');
						$('.form-contact-ajax-msg').hide();
						$('.form-contact-ajax-msg .form-contact-ajax-msg-sending').hide();
						$('.form-contact-submit-btn').show();
					}
				}
				else
				{
					console.log('fail sent');
					$('.form-contact-ajax-msg').hide();
					$('.form-contact-ajax-msg .form-contact-ajax-msg-sending').hide();
					$('.form-contact-submit-btn').show();
				}
			}
		});
		/*
		*/
		
		//try sending email notification
		/*
		$.ajax({
			url: "mail-notification.php",
			type: "POST",
			dataType: "json",
			data: o_params,
			success: function( o_response ) {
				console.log( o_response );				
			}
		});
		*/
		
		
		
		
	};
}]);
formApp.directive('compareTo', function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };
             scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
})






